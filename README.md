# Crow
Check Renovate (is) On (our) Webapps

## What it does
Creates a .csv file with three columns: Name, Has Renovate, and Number of Renovate-related merge requests. It uses the Gitlab API to run through all of our Prospero projects and check whether or not they have renovate and how many renovate merge requests they have.

## Setup
You will need a gitlab token set up and added to a `GITLAB_TOKEN` environment variable. How you do this is out of scope for this readme.

## Usage
`npm run start`
