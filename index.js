const https = require('node:https');
const fs = require('fs');

const token = process.env.GITLAB_TOKEN;

const options = {
    hostname: 'gitlab.com',
    port: 443,
    method: 'GET',
    headers: {
        'PRIVATE-TOKEN': token
    },
};

const outFile = 'out.json'

async function GetProjects() {
    const path = '/api/v4/groups/11257473/projects?order_by=name&sort=asc&per_page=100'
    var myOptions = options;
    options.path = path;
    var bigLongString = "";
    var projectArray;
    var projectIdArray = [];
    var promise = new Promise((resolve,reject) => {
        const req = https.request(myOptions, (res) => {
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                bigLongString += chunk;
            }); 

            res.on('end', () => {
                projectArray = JSON.parse(bigLongString);
              
                projectArray.forEach((project) => {
                    projectIdArray.push({id:project.id,name:project.name});
                })

                resolve(projectIdArray);
            });
        });

        req.on('error', (e) => {
            console.error(`problem with request: ${e.message}`);
            reject(`problem with request: ${e.message}`)
        });


        req.end();
    })
    return await promise;
}

async function GetProjectHasRenovate(id) {
    const path = `/api/v4/projects/${id}/repository/files/renovate.json?ref=develop`
    var myOptions = options;
    options.path = path;
    var bigLongString = "";
    var renovateFileData;
    var promise = new Promise((resolve,reject) => {
        const req = https.request(myOptions, (res) => {
            if(res.statusCode == 200){
                resolve(true);
            } else {
                resolve(false);
            }
        });

        req.on('error', (e) => {
            console.error(`problem with request: ${e.message}`);
            reject(`problem with request: ${e.message}`)
        });


        req.end();
    })
    return await promise;
}


async function GetProjectRenovateMergeRequests(id) {
    const path = `/api/v4/projects/${id}/merge_requests?state=opened`
    var myOptions = options;
    options.path = path;
    var bigLongString = "";
    var mrArray;
    var promise = new Promise((resolve,reject) => {
        const req = https.request(myOptions, (res) => {
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                bigLongString += chunk;
            }); 

            res.on('end', () => {
                mrArray = JSON.parse(bigLongString);
                var numberOfMRs = 0;
                mrArray.forEach((mr) => {
                    if(mr.author.name == "renovate-bot" || mr.title.includes("renovate")) {
                        numberOfMRs++;
                    }
                })
           
                resolve(numberOfMRs);
            });
        });

        req.on('error', (e) => {
            console.error(`problem with request: ${e.message}`);
            reject(`problem with request: ${e.message}`)
        });


        req.end();
    })
    return await promise;
}

function ConvertProjectsArrayToCSVString(projects) {
    var csv = "Name,Has Renovate,Number of Renovate MRs\n"
    projects.forEach(p => {
        csv += `${p.name},${p.hasRenovate},${p.renovateMergeRequest}\n`
    })
    return csv;
}

function WriteCSVStringToFile(csv) {
    fs.writeFile('out.csv', csv, err => {
        if(err) {
            console.error(err);
        } else {
            console.log("done")
        }
    })
}


async function Do() {
    var projects = await GetProjects();
    await sleep(100)
    for(let i = 0; i < projects.length; i++) {
        var project = projects[i];
        project.hasRenovate = await GetProjectHasRenovate(project.id);
        await sleep(100)
        project.renovateMergeRequest = await GetProjectRenovateMergeRequests(project.id);
        await sleep(100)
        projects[i] = project;
        process.stdout.write(`${Math.round(i/projects.length)*100}%\r`)
    }
    var csv = ConvertProjectsArrayToCSVString(projects);
    WriteCSVStringToFile(csv);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

Do();


